import { Component, OnInit, Inject } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';

let JSONDatas = [
  {"age": "21", "name":"Eshita"}
]
localStorage.setItem("infos", JSON.stringify(JSONDatas));
let info2 = JSON.parse(localStorage.getItem("infos2") || '{}');
let data = JSON.parse(localStorage.getItem("datas") || '{}');
console.log(info2+"***********"+data[0]);

export interface DialogData {
  age: string;
  name: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  age: string;
  name: string;
  data: any;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '550px',
      data: {name: this.name, age: this.age},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.data = result;
      console.log(this.data.name);
      console.log(this.data);
      info2.push([{"name":this.data.name, "age":this.data.age}]);
      localStorage.setItem('infos2', JSON.stringify(info2));
      console.log(info2);
      // this.age = this.data;
    });
  }

  ngOnInit(): void {
  }

}
